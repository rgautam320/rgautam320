<img src="https://user-images.githubusercontent.com/71542496/135060605-259f5229-45d1-4d33-a2b8-1da37d178b5f.gif">

<h2> Hey there! I'm Rajan Gautam.</h2>

<h3> 👨🏻‍💻 &nbsp;About Me </h3>

- 🤔 &nbsp; Exploring new technologies and developing software solutions and quick hacks.
- 🎓 &nbsp; Studying Computer Science and Engineering at Pandit Deendayal Energy University.
- 💼 &nbsp; Working as a Business Development Associate at VirtuBox InfoTech Private Limited.
- 🌱 &nbsp; Learning more about Blockchain Technology and Cyber Security.

<h3> 🛠 &nbsp;Tech Stack</h3>

- 💻 &nbsp;
  ![Python](https://img.shields.io/badge/-Python-333333?style=flat&logo=python)
  ![Java](https://img.shields.io/badge/-Java-333333?style=flat&logo=Java&logoColor=007396)
  ![C](https://img.shields.io/badge/-C-333333?style=flat&logo=C%2B%2B&logoColor=00599C)
  ![C++](https://img.shields.io/badge/-C++-333333?style=flat&logo=C%2B%2B&logoColor=00599C)
- 🌐 &nbsp;
  ![HTML](https://img.shields.io/badge/-HTML-333333?style=flat&logo=HTML)
  ![CSS](https://img.shields.io/badge/-CSS-333333?style=flat&logo=CSS3&logoColor=1572B6)
  ![JavaScript](https://img.shields.io/badge/-JavaScript-333333?style=flat&logo=javascript)
  ![Node.js](https://img.shields.io/badge/-Node.js-333333?style=flat&logo=node.js)
  ![React](https://img.shields.io/badge/-React-333333?style=flat&logo=react)
  ![Django](https://img.shields.io/badge/-Django-333333?style=flat&logo=django)
  ![Bootstrap](https://img.shields.io/badge/-Bootstrap-333333?style=flat&logo=bootstrap&logoColor=563D7C)
  ![Material UI](https://img.shields.io/badge/-MaterialUI-333333?style=flat&logo=material-ui&logoColor=563D7C)
- 🛢 &nbsp;
  ![MySQL](https://img.shields.io/badge/-MySQL-333333?style=flat&logo=mysql)
  ![PostgreSQL](https://img.shields.io/badge/-PostgreSQL-333333?style=flat&logo=postgresql)
  ![SQL Server](https://img.shields.io/badge/-SQLServer-333333?style=flat&logo=microsoft-sql-server)
  ![SQLite](https://img.shields.io/badge/-SQLite-333333?style=flat&logo=sqlite)
  ![MongoDB](https://img.shields.io/badge/-MongoDB-333333?style=flat&logo=mongodb)
- ⚙️ &nbsp;
  ![Git](https://img.shields.io/badge/-Git-333333?style=flat&logo=git)
  ![GitHub](https://img.shields.io/badge/-GitHub-333333?style=flat&logo=github)
  ![Azure](https://img.shields.io/badge/-Azure-333333?style=flat&logo=azure-devops)
  ![Markdown](https://img.shields.io/badge/-Markdown-333333?style=flat&logo=markdown)
- 🔧 &nbsp;
  ![Visual Studio Code](https://img.shields.io/badge/-Visual%20Studio%20Code-333333?style=flat&logo=visual-studio-code&logoColor=007ACC)
  ![Eclipse](https://img.shields.io/badge/-Eclipse-333333?style=flat&logo=eclipse-ide&logoColor=2C2255)
- 🖥 &nbsp;
  ![Photoshop](https://img.shields.io/badge/-Photoshop-333333?style=flat&logo=adobe-photoshop)
  ![Illustrator](https://img.shields.io/badge/-Illustrator-333333?style=flat&logo=adobe-illustrator)
  ![Premiere Pro](https://img.shields.io/badge/-PremierePro-333333?style=flat&logo=adobe-premiere-pro)

<br/>

<h3> 🤝🏻 &nbsp;Connect with Me </h3>

<p align="center">
<a href="https://www.rajangautam.com.np"><img alt="Website" src="https://img.shields.io/badge/Website-rajangautam.com.np-blue?style=flat-square&logo=google-chrome"></a>
<a href="https://www.instagram.com/rgautam320"><img alt="Instagram" src="https://img.shields.io/badge/Instagram-rgautam320-blue?style=flat-square&logo=instagram"></a>
<a href="https://www.facebook.com/rgautam320"><img alt="Facebook" src="https://img.shields.io/badge/Facebook-rgautam320-blue?style=flat-square&logo=facebook"></a>
<a href="https://www.twitter.com/rgautam320"><img alt="Twitter" src="https://img.shields.io/badge/Twitter-rgautam320-blue?style=flat-square&logo=twitter"></a>
<a href="https://www.linkedin.com/in/rgautam320/"><img alt="LinkedIn" src="https://img.shields.io/badge/LinkedIn-rgautam320-blue?style=flat-square&logo=linkedin"></a>
<a href="https://www.youtube.com/c/rgautam320/"><img alt="YouTube" src="https://img.shields.io/badge/YouTube-rgautam320-blue?style=flat-square&logo=youtube"></a>
<a href="https://www.gitlab.com/rgautam320/"><img alt="GitLab" src="https://img.shields.io/badge/GitLab-rgautam320-blue?style=flat-square&logo=gitlab"></a>
<a href="mailto:gautamrajan073@gmail.com"><img alt="Email" src="https://img.shields.io/badge/Email-rgautam320-blue?style=flat-square&logo=gmail"></a>
</p>
